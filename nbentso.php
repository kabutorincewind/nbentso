﻿<?php

function nbentso(){
    $handle = fopen("php://stdin", "r");
    fwrite($handle, "Stone-Scissors-Paper.\n");
    fwrite($handle, "Enter item (stone, scissors, paper).\n");
    $handles = ['stone', 'scissors', 'paper'];
    $move =  array_search( trim(fgets($handle)), $handles);
    fwrite($handle, "Press any key to throw response item.\n");
    fgets($handle);
    $backMove = (int) round(range(0, 2));

    fwrite($handle, "Your item $handles[$move] \n");
    fwrite($handle, "Responce item $handles[$backMove] \n");

    if($backMove === 0){
        if($move === 2){
            fwrite($handle, "paper covers stone.\n");
            fwrite($handle, "You won.\n");
        }
        if($move === 1){
            fwrite($handle, "scissors brokes by stone.\n");
            fwrite($handle, "You loose.\n");
        }
    }
    if($backMove === 1){
        if($move === 0){
            fwrite($handle, "stone brokes scissors.\n");
            fwrite($handle, "You won.\n");
        }
        if($move === 2){
            fwrite($handle, "paper cuts by scissors.\n");
            fwrite($handle, "You loose.\n");
        }
    }
    if($backMove === 2){
        if($move === 0){
            fwrite($handle, "stone covered by paper.\n");
            fwrite($handle, "You won.\n");
        }
        if($move === 1){
            fwrite($handle, "scissors cuts paper.\n");
            fwrite($handle, "You loose.\n");
        }
    }

    if($backMove === $move){
        fwrite($handle, 'Nobody won!');
    }
}

nbentso();
$handle = fopen('php://stdin', 'r');
fwrite($handle, 'Enter any key to start again.');
if(trim(fgets($handle)) !== ''){
    nbentso();
}